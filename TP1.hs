-- Pierrick Michel MICP17049305

module TP1 where 

import Data.List
import Data.Char
import Data.Function

type Annee = Int
type Mois = Int
type Jour = Int 
type Nom = String 
type Prenom = String
type Sigle = String
type Titre = String
type NbCredits = Int
type CodeSession = Int
type CodeProgramme = Int 
type CodeProf = String
type CodePermanent = String
type NoGroupe = Int
type MaxInscrip = Int
type Note = Int

data Date = Date Annee Mois Jour deriving (Show, Eq)

-- un cours est d�fini par un sigle, un titre et son nombre de cr�dits
data Cours = Cours Sigle Titre NbCredits deriving Show
-- une session est d�finie par le code de session, la date de d�but et la date de fin
data SessionUqam = SessionUqam CodeSession Date Date deriving Show
-- un professeur est d�fini par son code, son nom et son pr�nom
data Professeur = Professeur CodeProf Nom Prenom deriving Show
-- un groupe cours est d�fini par le sigle du cours, le num�ro du groupe, le code de la session, le nombre maximal d'inscriptions et le code du professeur
data GroupeCours = GroupeCours Sigle NoGroupe CodeSession MaxInscrip CodeProf deriving Show
-- un �tudiant est d�fini par son code permanent, son nom, son prenom et son code programme
data Etudiant = Etudiant CodePermanent Nom Prenom CodeProgramme deriving Show
-- une inscription est d�finie par le code permanent de l'�tudiant, un sigle de cours, un numero de groupe, un code de session, une date d'inscription, une date d'Abandon et une note
data Inscription = Inscription CodePermanent Sigle NoGroupe CodeSession Date Date Note deriving (Show, Eq)
getAnnee (Date annee _ _) = annee
getMois (Date _ mois _) = mois
getJour (Date _ _ jour) = jour

instance Ord Date where
                        compare x y
                            | (getAnnee x == getAnnee y) && (getMois x == getMois y) && (getJour x == getJour y) = EQ
                            | (getAnnee y > getAnnee x) || ((getAnnee y == getAnnee x) && getMois y > getMois y) || ((getAnnee y == getAnnee x) && (getMois y == getMois y) && getJour y > getJour x) = GT
                            | otherwise = LT

--fonction simple qui calcule le nombre de jours entre deux dates--
nombreJours (Date annee1 mois1 jour1) (Date annee2 mois2 jour2) = abs((abs(annee1 - annee2)*12) - abs(mois1 - mois2))*30 + abs(jour1 - jour2)
                            
member x [] = False
member x (y:ys) | x==y  = True
                | otherwise = member x ys

-- ***********************************************************************************************************--
-- *********************************** VOUS DEVEZ COMPLETEZ CETTE PARTIE *************************************--
-- ***********************************************************************************************************--

{-	1-	La fonction numeroGroupes retourne la liste comprenant les num�ros de groupe du sigle INF3180 � la session de numero 12004 
    Entr�s: Liste de GroupeCours,
    Sortie: Liste contenant des numeros de groupe	-}

numeroGroupes :: [GroupeCours] -> [NoGroupe]
numeroGroupes [] = []
numeroGroupes ((GroupeCours sigle nogroupe codesession _ _):xs) = if ((sigle == "INF3180") && (codesession == 12004)) then ([nogroupe] ++ (numeroGroupes xs)) else (numeroGroupes xs)
    
{-	2-	La fonction siglesCours retourne la liste des sigles des cours (sans r�p�tition) donn�s � la session 32003 
    Entr�s: Liste de GroupeCours, liste des sigles (initialement vide)
    Sortie: Liste contenant les sigles des cours	-}

siglesCours :: [GroupeCours] -> [Sigle] -> [Sigle]
siglesCours [] ys = ys
siglesCours ((GroupeCours sigle nogroupe codesession maxinscrip codeprof):xs) ys = if ((codesession == 32003) && (not(isElem sigle ys))) then (siglesCours xs (ys++[sigle])) else (siglesCours xs ys)

-- V�rifie si un �l�ment est dans une liste
isElem :: (Eq a) => a -> [a] -> Bool
isElem x [] = False
isElem x (y:ys) = if (x == y) then True else (isElem x ys)

{- 3-  La fonction numGroupesCoursEtu retourne la liste des numeros de groupes des cours suivis par un �tudiant X � la session dont le codeSession est 32003
   Entr�s: liste des inscriptions et un �tudiant
   Sortie: liste des numeros de groupes   -}

numGroupesCoursEtu :: [Inscription] -> Etudiant -> [NoGroupe]
numGroupesCoursEtu [] _ = []
numGroupesCoursEtu ((Inscription codepermanent sigle nogroupe codesession datee date note):xs) (Etudiant etucodepermanent etunom etuprenom etucodeprogramme) = if ((codesession == 32003) && (codepermanent == etucodepermanent)) then ([nogroupe] ++ (numGroupesCoursEtu xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme))) else (numGroupesCoursEtu xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme))

{- 4- La fonction codePermMoy80, qui pour chaque �tudiant dont la moyenne est sup�rieure � 80 pour les cours suivis � la session 32003, affiche le code permanent 
   Entr�s: liste des inscriptions, liste des �tudiants 
   Sortie: liste des codes permanent-}

codePermMoy80 :: [Inscription] -> [Etudiant] -> [CodePermanent]
codePermMoy80 xs [] = []
codePermMoy80 xs ((Etudiant etucodepermanent etunom etuprenom etucodeprogramme):ys) = if ((div (checkSum xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme)) (lengthCon xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme))) > 80) then ([etucodepermanent] ++ (codePermMoy80 xs ys)) else (codePermMoy80 xs ys)

-- Calcule la somme d'un �tudiant � la session 32003
checkSum :: [Inscription] -> Etudiant -> Int
checkSum [] y = 0
checkSum ((Inscription codepermanent sigle nogroupe codesession datee date note):xs) (Etudiant etucodepermanent etunom etuprenom etucodeprogramme) = if (codesession == 32003 && codepermanent == etucodepermanent) then (note + checkSum xs ((Etudiant etucodepermanent etunom etuprenom etucodeprogramme))) else (0 + checkSum xs ((Etudiant etucodepermanent etunom etuprenom etucodeprogramme)))

-- Compte la longueur des inscriptions � la session 32003
lengthCon :: [Inscription] -> Etudiant -> Int
lengthCon [] y = 0
lengthCon ((Inscription codepermanent sigle nogroupe codesession datee date note):xs) (Etudiant etucodepermanent etunom etuprenom etucodeprogramme) = if ((codesession == 32003) && (codepermanent == etucodepermanent)) then (1 + (lengthCon xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme))) else ( 0 + (lengthCon xs (Etudiant etucodepermanent etunom etuprenom etucodeprogramme)))

{- 5- La fonction profNbcredits, pour chacun des professeurs, affiche son nom, pr�nom et nombre de cr�dits de cours donn�s (il faut additionner les cr�dits de cours de chacun des groupes auquel le professeur est assign�).
   Entr�s: Liste de professeurs, la liste des groupes cours et la liste des cours
   Sortie: Liste de triplets de la forme (nom prof, prenom prof, nb de cr�dits)   -}

profNbcredits :: [Professeur] -> [GroupeCours] -> [Cours] -> [(Nom, Prenom, NbCredits)]
profNbcredits [] ys zs = []
profNbcredits ((Professeur codeprof nom prenom):xs) ys zs = [(nom, prenom, sumCredits (Professeur codeprof nom prenom) ys zs)] ++ (profNbcredits xs ys zs)

-- Calcule la somme des cr�dits d'un professeur
sumCredits :: Professeur -> [GroupeCours] -> [Cours] -> Int
sumCredits _ [] [] = error "Pas de GroupeCours dans sumCredits"
sumCredits _ _ [] = error "Pas de Cours dans sumCredits"
sumCredits (Professeur profcodeprof profnom profprenom) [] zs = 0
sumCredits (Professeur profcodeprof profnom profprenom) ((GroupeCours sigle nogroupe codesession maxinscrip codeprof):ys) zs = if (profcodeprof == codeprof) then (( getCoursCredits sigle zs ) + sumCredits (Professeur profcodeprof profnom profprenom) ys zs) else (sumCredits (Professeur profcodeprof profnom profprenom) ys zs)

-- Obtenir le nombre de cr�dits d'un cours � partir d'un sigle (pris d'un GroupeCours)
getCoursCredits :: Sigle -> [Cours] -> Int
getCoursCredits x [] = error "Un des sigles des [GroupeCours] ne correspond pas a un cours"
getCoursCredits x ((Cours sigle titre nbcredits):ys) = if (x == sigle) then nbcredits else getCoursCredits x ys


{- 6- La fonction detailsAbandons affiche les d�tails des abandons qui ont eu lieu plus de deux semaines (15 jours et plus) apr�s le d�but de la session. Afficher le codePermanent, sigle, noGroupe, codeSession, Date d'abandon, et nombre de jours entre l�abandon et le d�but de la session
   Entr�s: Liste des inscriptions et liste des sessions Uqam
   Sortie: Liste de sixtuplets de la forme (codePermanent, sigle, noGroupe, codeSession, Date d'abandon, nombre de jours entre l�abandon et le d�but de la session) -}

detailsAbandons :: [Inscription] -> [SessionUqam] -> [(CodePermanent, Sigle, NoGroupe, CodeSession, Date, Int)]
detailsAbandons [] ys = []
detailsAbandons xs [] = []
detailsAbandons ((Inscription codepermanent sigle nogroupe codesession date dateabandon note):xs) ys = if (not(dateabandon == (Date 0 0 0)) && (legitAbandon dateabandon codesession ys >= 15)) then [(codepermanent, sigle, nogroupe, codesession, dateabandon, legitAbandon dateabandon codesession ys)] ++ (detailsAbandons xs ys) else detailsAbandons xs ys 

-- Calcule le nombre de jours entre une date d'abandon et le debut d'une session
legitAbandon :: Date -> CodeSession -> [SessionUqam] -> Int
legitAbandon dateabandon codesession [] = 0
legitAbandon dateabandon codesession ((SessionUqam uqamcodesession uqamdate uqamdateabandon):ys) = if codesession == uqamcodesession then nombreJours dateabandon uqamdate else legitAbandon dateabandon codesession ys
   
{- 7- La fonction groupeSansInscrp affiche la liste des sigle, noGroupe et codeSession des groupes qui n�ont pas d�inscription 
   Entr�s: liste des groupes cours et liste des inscriptions
   Sortie: liste de triplets de la forme (sigle, noGroupe, codeSession)   -}

groupeSansInscrp :: [GroupeCours] -> [Inscription] -> [(Sigle, NoGroupe, CodeSession)]
groupeSansInscrp [] ys = []
groupeSansInscrp ((GroupeCours sigleCours nogroupe codesessionCours maxinscrip codeprof):xs) ys = if not(hasInscription (GroupeCours sigleCours nogroupe codesessionCours maxinscrip codeprof) ys) then [(sigleCours, nogroupe, codesessionCours)] ++ (groupeSansInscrp xs ys) else groupeSansInscrp xs ys


hasInscription :: GroupeCours -> [Inscription] -> Bool
hasInscription x [] = False
hasInscription (GroupeCours sigleCours nogroupeCours codesessionCours maxinscrip codeprof) ((Inscription codepermanent sigle nogroupe codesession date dateAbandon note):ys) = if ((sigle == sigleCours) && (nogroupe == nogroupeCours) && (codesession == codesessionCours)) then True else (hasInscription (GroupeCours sigleCours nogroupeCours codesessionCours maxinscrip codeprof) ys)

{- 8- La fonction etudiantsBlaise affiche la liste des noms et pr�noms (sans r�p�tition) de tous les �tudiants qui ont suivi au moins un cours avec le professeur Blaise Pascal (NB On suppose qu�il n�y a qu�un professeur nomm� Blaise Pascal). 
   Entr�s: liste des �tudiants, liste des inscriptions, liste des groupes et liste des professeurs
   Sortie: liste de couples de la forme (nom etudiant, pr�nom �tudiant) -}

etudiantsBlaise :: [Etudiant] -> [Inscription] -> [GroupeCours] -> [Professeur] -> [(Nom, Prenom)]
etudiantsBlaise [] xs ys zs = []
etudiantsBlaise ((Etudiant codepermanent nom prenom codeprogramme):ws) xs ys zs = if (isCours (Etudiant codepermanent nom prenom codeprogramme) xs ys (getBlaisePascal zs)) then ([(nom, prenom)] ++ etudiantsBlaise ws xs ys zs) else (etudiantsBlaise ws xs ys zs)

--Est-ce que l'�tudiant a un cours avec prof
isCours :: Etudiant -> [Inscription] -> [GroupeCours] -> CodeProf -> Bool
isCours w [] ys z = False
isCours (Etudiant etucodepermanent etunom etuprenom etucodeprogramme) ((Inscription codepermanent sigle nogroupe codesession date dateAbandon note):xs) ys z = if ((etucodepermanent == codepermanent) && (isGroupeCours sigle nogroupe codesession ys z)) then True else (isCours (Etudiant etucodepermanent etunom etuprenom etucodeprogramme) xs ys z)

--Est-ce que le cours est le bon groupeCours 
isGroupeCours :: Sigle -> NoGroupe -> CodeSession -> [GroupeCours] -> CodeProf -> Bool
isGroupeCours v w x [] z = False
isGroupeCours v w x ((GroupeCours sigle nogroupe codesession maxinscrip codeprof):ys) z = if ((v == sigle) && (w == nogroupe) && (x == codesession) && (z == codeprof)) then True else (isGroupeCours v w x ys z)

-- Obtenir le code prof du professeur Blaise Pascal; grand math�maticien, physicien, inventeur, philosophe, moraliste et th�ologien fran�ais du 17e si�cle
getBlaisePascal :: [Professeur] -> CodeProf
getBlaisePascal [] = error "Blaise Pascal n'est pas dans la liste des professeurs"
getBlaisePascal ((Professeur codeprof nom prenom):xs) = if (nom == "Pascal" && prenom == "Blaise") then codeprof else (getBlaisePascal xs)