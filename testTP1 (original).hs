import TP1
import System.Environment
import System.IO

cours1 = (TP1.Cours "INF3180" "Fichiers et bases de donnees" 3)        
cours2 = (TP1.Cours "INF5180" "Conception et exploitation d'une base de donnees" 3)           
cours3 = (TP1.Cours "INF1110" "Programmation I" 3)         
cours4 = (TP1.Cours "INF1130" "Mathematiques pour informaticien" 3)          
cours5 = (TP1.Cours "INF2110" "Programmation II" 3)           
cours6 = (TP1.Cours "INF3123" "Programmation objet" 3)   
listecours = [cours1, cours2, cours3, cours4, cours5, cours6]    

sessionUqam1 = (TP1.SessionUqam 32003 (Date 2003 9 3) (Date 2003 12 17))                                              
sessionUqam2 = (TP1.SessionUqam 12004 (Date 2004 1 8) (Date 2004 5 2))
listesessionuqam  = [sessionUqam1, sessionUqam2]

groupeCours1 = (TP1.GroupeCours "INF1110" 20 32003 100 "TREJ4")                            
groupeCours2 = (TP1.GroupeCours "INF1110" 30 32003 100 "PASB1")                            
groupeCours3 = (TP1.GroupeCours "INF1130" 10 32003 100 "PASB1")                            
groupeCours4 = (TP1.GroupeCours "INF1130" 30 32003 100 "GALE9")                            
groupeCours5 = (TP1.GroupeCours "INF2110" 10 32003 100 "TREJ4")                            
groupeCours6 = (TP1.GroupeCours "INF3180" 30 32003 50 "DEVL2")                            
groupeCours7 = (TP1.GroupeCours "INF3180" 40 32003 50 "DEVL2")                            
groupeCours8 = (TP1.GroupeCours "INF1110" 20 12004 100 "TREJ4")                            
groupeCours9 = (TP1.GroupeCours "INF1110" 30 12004 100 "TREJ4")                            
groupeCours10 = (TP1.GroupeCours "INF2110" 10 12004 100 "PASB1")                            
groupeCours11 = (TP1.GroupeCours "INF2110" 40 12004 100 "PASB1")                      
groupeCours12 = (TP1.GroupeCours "INF3180" 10 12004 50 "DEVL2")                            
groupeCours13 = (TP1.GroupeCours "INF3180" 30 12004 50 "DEVL2")                            
groupeCours14 = (TP1.GroupeCours "INF5180" 10 12004 50 "DEVL2")                            
groupeCours15 = (TP1.GroupeCours "INF5180" 40 12004 50 "GALE9") 
listegroupecours = [groupeCours1, groupeCours2, groupeCours3, groupeCours4, groupeCours5, groupeCours6, groupeCours7, groupeCours8, groupeCours9, groupeCours10, groupeCours11, groupeCours12, groupeCours13, groupeCours14, groupeCours15]                           

inscription1 = (TP1.Inscription "TREJ18088001" "INF1110" 20 32003 (Date 2003 8 16) (Date 0 0 0) 80)  
inscription2 = (TP1.Inscription "LAVP08087001" "INF1110" 20 32003 (Date 2003 8 16) (Date 0 0 0) 80)    
inscription3 = (TP1.Inscription "TREL14027801" "INF1110" 30 32003 (Date 2003 8 17) (Date 0 0 0) 90)    
inscription4 = (TP1.Inscription "MARA25087501" "INF1110" 20 32003 (Date 2003 8 20) (Date 0 0 0) 80)    
inscription5 = (TP1.Inscription "STEG03106901" "INF1110" 20 32003 (Date 2003 8 17) (Date 0 0 0) 70)   
inscription6 = (TP1.Inscription "TREJ18088001" "INF1130" 10 32003 (Date 2003 8 16) (Date 0 0 0) 70)    
inscription7 = (TP1.Inscription "TREL14027801" "INF1130" 30 32003 (Date 2003 8 17) (Date 0 0 0) 80)    
inscription8 = (TP1.Inscription "MARA25087501" "INF1130" 10 32003 (Date 2003 8 22) (Date 0 0 0) 90)    
inscription9 = (TP1.Inscription "DEGE10027801" "INF3180" 30 32003 (Date 2003 8 16) (Date 0 0 0) 90)    
inscription10 = (TP1.Inscription "MONC05127201" "INF3180" 30 32003 (Date 2003 8 19) (Date 0 0 0) 60)    
inscription11 = (TP1.Inscription "VANV05127201" "INF3180" 30 32003 (Date 2003 8 16) (Date 2003 9 20) 0)               
inscription12 = (TP1.Inscription "EMEK10106501" "INF3180" 40 32003 (Date 2003 8 19) (Date 0 0 0) 80)    
inscription13 = (TP1.Inscription "DUGR08085001" "INF3180" 40 32003 (Date 2003 8 19) (Date 0 0 0) 70)   
inscription14 = (TP1.Inscription "TREJ18088001" "INF2110" 10 12004 (Date 2003 12 19) (Date 0 0 0) 80)   
inscription15 = (TP1.Inscription "TREL14027801" "INF2110" 10 12004 (Date 2003 12 20) (Date 0 0 0) 90)    
inscription16 = (TP1.Inscription "MARA25087501" "INF2110" 40 12004 (Date 2003 12 19) (Date 0 0 0) 90)   
inscription17 = (TP1.Inscription "STEG03106901" "INF2110" 40 12004 (Date 2003 12 10) (Date 0 0 0) 70)    
inscription18 = (TP1.Inscription "VANV05127201" "INF3180" 10 12004 (Date 2003 12 18) (Date 0 0 0) 90)    
inscription19 = (TP1.Inscription "DEGE10027801" "INF5180" 10 12004 (Date 2003 12 15) (Date 0 0 0) 90)    
inscription20 = (TP1.Inscription "MONC05127201" "INF5180" 10 12004 (Date 2003 12 19) (Date 2004 1 22) 0)              
inscription21 = (TP1.Inscription "EMEK10106501" "INF5180" 40 12004 (Date 2003 12 19) (Date 0 0 0) 80)    
inscription22 = (TP1.Inscription "DUGR08085001" "INF5180" 10 12004 (Date 2003 12 19) (Date 0 0 0) 80) 
listeinscriptions = [inscription1, inscription2, inscription3, inscription4, inscription5, inscription6, inscription7, inscription8, inscription9, inscription10, inscription11, inscription12, inscription13, inscription14, inscription15,inscription16,inscription17,inscription18,inscription19,inscription20,inscription21,inscription22]   

etudiant1 = (TP1.Etudiant "TREJ18088001" "Tremblay" "Jean" 7416)
etudiant2 = (TP1.Etudiant "TREL14027801" "Tremblay" "Lucie" 7416)
etudiant3 = (TP1.Etudiant "DEGE10027801" "Degas" "Edgar" 7416)
etudiant4 = (TP1.Etudiant "MONC05127201" "Monet" "Claude" 7316)
etudiant5 = (TP1.Etudiant "VANV05127201" "Van Gogh" "Vincent" 7316)
etudiant6 = (TP1.Etudiant "MARA25087501" "Marshall" "Amanda" 0)
etudiant7 = (TP1.Etudiant "STEG03106901" "Stephani" "Gwen" 7416)
etudiant8 = (TP1.Etudiant "EMEK10106501" "Emerson" "Keith" 7416)
etudiant9 = (TP1.Etudiant "DUGR08085001" "Duguay" "Roger" 0)
etudiant10 = (TP1.Etudiant "LAVP08087001" "Lavoie" "Paul" 0)
listeetudiants = [etudiant1, etudiant2, etudiant3,etudiant4,etudiant5,etudiant6,etudiant7,etudiant8,etudiant9,etudiant10] 

professeur1 = (TP1.Professeur "TREJ4" "Tremblay" "Jean")
professeur2 = (TP1.Professeur "DEVL2" "De Vinci" "Leonard")
professeur3 = (TP1.Professeur "PASB1" "Pascal" "Blaise")
professeur4 = (TP1.Professeur "GOLA1" "Goldberg" "Adele")
professeur5 = (TP1.Professeur "KNUD1" "Knuth" "Donald")
professeur6 = (TP1.Professeur "GALE9" "Galois" "Evariste")
professeur7 = (TP1.Professeur "CASI0" "Casse" "Illa")
listeprofesseurs = [professeur1,professeur2,professeur3,professeur4,professeur5,professeur6,professeur7]      

  
main = do
     print "************Test du TP1*****************"
     print "****************MENU********************"
     putStrLn ( "test1 = " ++ show (test1))
     putStrLn ( "test2 = " ++ show (test2))
     putStrLn ( "test3a = " ++ show (test3a))
     putStrLn ( "test3b = " ++ show (test3b))
     putStrLn ( "test4 = " ++ show (test4))
     putStrLn ( "test5 = " ++ show (test5))
     putStrLn ( "test6 = " ++ show (test6))
     putStrLn ( "test7 = " ++ show (test7))
     putStrLn ( "test8 = " ++ show (test8))

test1 = TP1.numeroGroupes listegroupecours == [10,30]
test2 = TP1.siglesCours listegroupecours [] == ["INF1110","INF1130","INF2110","INF3180"]
test3a = TP1.numGroupesCoursEtu listeinscriptions etudiant1 == [20,10]
test3b = TP1.numGroupesCoursEtu listeinscriptions etudiant5 == [30]
test4 = TP1.codePermMoy80 listeinscriptions listeetudiants == ["TREL14027801","DEGE10027801","MARA25087501"]
test5 = TP1.profNbcredits listeprofesseurs listegroupecours listecours == [("Tremblay","Jean",12),("De Vinci","Leonard",15),("Pascal","Blaise",12),("Goldberg","Adele",0),("Knuth","Donald",0),("Galois","Evariste",6),("Casse","Illa",0)]
test6 = TP1.detailsAbandons listeinscriptions listesessionuqam == [("VANV05127201","INF3180",30,32003,Date 2003 9 20,17)]
test7 = TP1.groupeSansInscrp listegroupecours listeinscriptions == [("INF2110",10,32003),("INF1110",20,12004),("INF1110",30,12004),("INF3180",30,12004)]
test8 = TP1.etudiantsBlaise listeetudiants listeinscriptions listegroupecours listeprofesseurs == [("Tremblay","Jean"),("Tremblay","Lucie"),("Marshall","Amanda"),("Stephani","Gwen")]
